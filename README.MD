# WP Coop CE Theme

CE's child theme to be used with WP Coop Theme

## Downloads
Visit [releases](../../releases) page or get the [latest](../../releases/permalink/latest/downloads/themes/wp-coop-ce-theme.zip) zip.

> Releases are generated automatically by CI/CD pipeline every semver compatible git tag. In order to trigger the pipeline, tag the commit you want with the [proper next version](https://semver.org/).

## General

Please visit [WP Coop Theme](https://gitlab.com/coopdevs/wp-coop-theme) for more detailed documentation

## Development

* Run `npm install`
* Run `npm run watch` to start developing

## Contributors

* [Coopdevs](https://coopdevs.org)
* [Som Energia](https://somenergia.coop)
* [Talaios](https://talaios.coop)

## License

MIT. Please see the [License File](https://gitlab.com/coopdevs/wp-coop-theme/-/blob/main/LICENSE) for more information.
